import json
import os
import zipfile

from ..models import Network


def unzip(src_path: str, dest_path: str, exp) -> bool:
    if not zipfile.is_zipfile(src_path):
        exp.status = "Wrong format : zip file expected"
        exp.save()
        return False

    with zipfile.ZipFile(src_path, 'r') as zip_ref:
        zip_ref.extractall(dest_path)

    return True


def contains_basic_files(path: str, exp) -> bool:
    if not os.path.isdir(path):
        exp.status = "Wrong format : zip should contain a folder"
        exp.save()
        return False

    required_files = ["amas.py", "env.py", "config.json"]
    for required_file in required_files:
        if not os.path.exists(path + "/" + required_file):
            exp.status = "Wrong format : " + required_file + " file is expected"
            exp.save()
            return False
    return True


def check_config(path: str, exp) -> bool:
    with open(path + "/config.json") as json_file:
        config = json.load(json_file)

    required_keys = ["iotamak_version", "scheduling_type"]
    for key in required_keys:
        if key not in config:
            exp.status = "Wrong config : " + key + " is expected in config.json"
            exp.save()
            return False

    if config.get("scheduling_type") not in ["sync", "async"]:
        exp.status = "Wrong config : scheduling_type is either sync or async"
        exp.save()
        return False

    n = Network.objects.all()[0]
    version = n.iotamak_core_version

    if int(config.get("iotamak_version").replace(".", "00")) < int(version.replace(".", "00")):
        exp.status = "Wrong version : experiment version is outdated"
        exp.save()
        return False
    elif int(config.get("iotamak_version").replace(".", "00")) > int(version.replace(".", "00")):
        exp.status = "Wrong version : experiment version is ahead of the server version"
        exp.save()
        return False

    if config.get("scheduling_type") == "sync":
        if not os.path.exists(path + "/scheduler.py"):
            exp.status = "Wrong format : scheduler.py file is expected"
            exp.save()
            return False
    else:
        pass

    return True
