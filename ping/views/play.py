import csv
import os
import random
import sys
import zipfile
import json

from ast import literal_eval
from subprocess import Popen
from time import sleep

from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.template import loader
from django.urls import reverse
from iotAmak.tool.ssh_client import Cmd
from paho.mqtt.client import Client as MQTTClient

from .tool import delete_folder, get_ssh_client, canvas_event_triger, graph_event_triger, com_event_triger
from ..models import Experiment, CurrentExperiment, Metrics, Network, Link, Node, Canvas, GraphPoint

client = MQTTClient(client_id="django-ihm")


def subscribe(mqttclient, topic, fun):
    mqttclient.subscribe(topic)
    mqttclient.message_callback_add(topic, fun)


def agent_log(client, userdata, message) -> None:
    """
    Called when the amas receive a log from any agent, print it in stdout
    """
    print("[Log] " + str(message.payload.decode("utf-8")) + " on topic " + message.topic)


def agent_message(client, userdata, message) -> None:
    id1 = literal_eval(message.payload.decode("utf-8")).get("id")
    id2 = int(message.topic.split("/")[1])

    if id1 > id2:
        id1, id2 = id2, id1

    link = Link.objects.filter(id1=id1, id2=id2)[0]
    if link.value < 30:
        link.value += 1
    link.save()


def agent_metric(client, userdata, message) -> None:
    results = literal_eval(message.payload.decode("utf-8"))

    exp = CurrentExperiment.objects.all()[0]
    metrics = Metrics(cycle=exp.cycle, metrics=results)
    metrics.save()


def agent_canvas(client, userdata, message) -> None:
    results = literal_eval(message.payload.decode("utf-8"))

    canvas = Canvas(
        agent_id=results.get("id"),
        x=results.get("x"),
        y=results.get("y"),
        color=results.get("color"),
        cycle=results.get("cycle")
    )
    canvas.save()


def evaporation():
    links = Link.objects.all()

    for link in links:
        link.value -= 1
        if link.value <= 0:
            link.value = 0
        link.save()


def cycle_done(client, userdata, message) -> None:
    cur_exp = CurrentExperiment.objects.all()[0]

    if cur_exp.com_graph:
        if cur_exp.cycle % cur_exp.com_refresh == 0:
            for _ in range(cur_exp.com_refresh):
                evaporation()

            data = {
                "nodes": [
                    {"id": node.name, "group": node.group}
                    for node in Node.objects.all()
                ],
                "links": [
                    {
                        "source": "Agent_" + str(elem.id1),
                        "target": "Agent_" + str(elem.id2),
                        "value": elem.value
                    }
                    for elem in Link.objects.all()
                ]
            }
            com_event_triger(data)

    if cur_exp.canvas:
        canvas = [
            {"color": elem.color, "x": elem.x, "y": elem.y}
            for elem in Canvas.objects.filter(cycle=cur_exp.cycle)
        ]

        canvas_event_triger(canvas)

    if cur_exp.graph and (cur_exp.cycle % cur_exp.graph_refresh == 0):
        data = [
            {"name": elem.name, "x":elem.cycle, "y":elem.y}
            for elem in GraphPoint.objects.all()
        ]
        graph_event_triger(data)


    cur_exp.cycle += 1
    cur_exp.save()


def update_nbr_agent(client, userdata, message) -> None:
    results = literal_eval(message.payload.decode("utf-8"))
    exp = CurrentExperiment.objects.all()[0]

    subscribe(client, "agent/" + str(results.get("id")) + "/log", agent_log)
    if exp.com_graph:
        subscribe(client, "agent/" + str(results.get("id")) + "/mail", agent_message)
        node = Node(name="Agent_" + str(results.get("id")), group=results.get("ip"))
        node.save()
        for i in range(exp.nbr_agent):
            link = Link(id1=i, id2=results.get("id"))
            link.save()

    if exp.canvas:
        subscribe(client, "agent/" + str(results.get("id")) + "/canvas", agent_canvas)

    exp.nbr_agent += 1
    exp.save()

def agent_graph(client, userdata, message) -> None:
    results = literal_eval(message.payload.decode("utf-8"))

    for elem in results.get("point"):
        point = GraphPoint(name=elem.get("name"),cycle=results.get("cycle"),y=elem.get("value"))
        point.save()

def experiment_load(request):
    delete_folder(str(settings.MEDIA_ROOT) + "current_experiment")

    exp = Experiment.objects.get(status="Selected")

    with zipfile.ZipFile(str(settings.MEDIA_ROOT) + str(exp.media), 'r') as zip_ref:
        zip_ref.extractall(str(settings.MEDIA_ROOT) + "current_experiment")

    # drop metrics table
    Metrics.objects.all().delete()

    # drop current experiment table and create new instance
    CurrentExperiment.objects.all().delete()
    cur_exp = CurrentExperiment(name=Experiment.objects.get(status="Selected").name)

    with open(str(settings.MEDIA_ROOT) + "current_experiment/" + str(exp.name) + "/config.json") as json_file:
        config = json.load(json_file)

    if "seed" in config:
        cur_exp.seed = str(config.get("seed"))
    else:
        random.seed()
        cur_exp.seed = str(random.randint(0, 9999999))

    if "com_graph" in config:
        cur_exp.com_graph = True
        cur_exp.com_fun = config.get("com_graph").get("fun")
        cur_exp.com_refresh = config.get("com_graph").get("refresh")
        Link.objects.all().delete()
        Node.objects.all().delete()

    if "canvas" in config:
        cur_exp.canvas = True
        cur_exp.canvas_height = config.get("canvas").get("height")
        cur_exp.canvas_width = config.get("canvas").get("width")
        Canvas.objects.all().delete()

    if "graph" in config:
        cur_exp.graph = True
        cur_exp.graph_height = config.get("graph").get("height")
        cur_exp.graph_width = config.get("graph").get("width")
        cur_exp.graph_refresh = config.get("graph").get("refresh")
        GraphPoint.objects.all().delete()

    cur_exp.scheduling = config.get("scheduling_type")

    cur_exp.save()

    return HttpResponseRedirect(reverse('ping:play'))


def delete_nohup():
    ssh = get_ssh_client()

    commands = [
        Cmd(
            cmd="rm nohup.out"
        )

    ]
    for i_client in range(len(ssh.clients)):
        print(" Ip : ", ssh.clients[i_client].hostname)
        ssh.run_cmd(i_client, commands)


def get_nohup():
    ssh = get_ssh_client()

    commands = [
        Cmd(
            cmd="cat nohup.out"
        )

    ]
    for i_client in range(len(ssh.clients)):
        print(" Ip : ", ssh.clients[i_client].hostname)
        ssh.run_cmd(i_client, commands)


def experiment_start(request):
    n = Network.objects.all()[0]
    ssh = get_ssh_client()

    experiment_path = str(settings.MEDIA_ROOT) + "current_experiment/" + Experiment.objects.get(status="Selected").name

    # Connect to the broker
    global client
    client.disconnect()
    client = MQTTClient(client_id="django-ihm")
    client.username_pw_set(username=n.broker_username, password=n.broker_password)
    client.connect(host=n.broker_ip)
    client.loop_start()
    subscribe(client, "amas/agent/new", update_nbr_agent)
    subscribe(client, "scheduler/cycledone", cycle_done)
    subscribe(client, "amas/graph", agent_graph)
    subscribe(client, "amas/all_metric", agent_metric)

    delete_nohup()

    exp = CurrentExperiment.objects.all()[0]

    if exp.scheduling == "sync":
        p1 = Popen(
            [sys.executable, experiment_path + '/scheduler.py', n.broker_ip, n.broker_username, n.broker_password]
        )
        exp.scheduler_pid = p1.pid
        sleep(1)
    # start subprocess amas
    amas_dict = {
        "broker_ip": n.broker_ip,
        "clients": str([c.to_send() for c in ssh.clients]),
        "seed": exp.seed,
        "iot_path": n.path_to_iotamak + "example/",
        "broker_username": n.broker_username,
        "broker_password": n.broker_password,
        "experiment_folder": exp.name
    }
    if not exp.scheduling == "sync":
        amas_dict["wait_delay"] = str(5)
    p2 = Popen([sys.executable, experiment_path + '/amas.py', json.dumps(amas_dict)])
    # start subprocess env
    env_dict = {
        "broker_ip": n.broker_ip,
        "seed": exp.seed,
        "broker_username": n.broker_username,
        "broker_password": n.broker_password
    }
    if not exp.scheduling == "sync":
        env_dict["wait_delay"] = str(5)
    p3 = Popen([sys.executable, experiment_path + '/env.py', json.dumps(env_dict)])

    exp.amas_pid = p2.pid
    exp.env_pid = p3.pid
    exp.save()

    return HttpResponseRedirect(reverse('ping:play'))


def main_play(request):
    context = {}
    canvas = []
    metrics = []
    com_graph = False

    if len(CurrentExperiment.objects.all()) > 0:
        exp = CurrentExperiment.objects.all()[0]
        cycle = exp.cycle
        if exp.canvas:
            canvas = [exp.canvas_height, exp.canvas_width]
        com_graph = exp.com_graph

    else:
        cycle = 0
    for i in range(cycle):
        # get all the agent 0 metrics
        raw_context = Metrics.objects.filter(cycle=i)[0].metrics

        cycle_metrics = [[metric.get("x"), metric.get("y")] for metric in raw_context if metric != {}]

        metrics.append(cycle_metrics)

    context["metrics"] = metrics
    context["canvas"] = canvas
    context["com_graph"] = com_graph

    template = loader.get_template('ping/play.html')
    return HttpResponse(template.render(context, request))


def scheduler_step(request):
    client.publish("ihm/step")
    return HttpResponseRedirect(reverse('ping:play'))


def scheduler_stop(request):
    client.publish("ihm/pause")
    return HttpResponseRedirect(reverse('ping:play'))


def scheduler_start(request):
    client.publish("ihm/unpause")
    return HttpResponseRedirect(reverse('ping:play'))


def experiment_share(request):
    # get selected experiment
    exp = Experiment.objects.get(status="Selected")
    n = Network.objects.all()[0]

    # open ssh connection
    ssh = get_ssh_client()

    # file transfer
    commands = [
        Cmd(
            cmd="rm -r " + n.path_to_iotamak + "example/" + exp.name
        )

    ]
    for i_client in range(len(ssh.clients)):
        print(" Ip : ", ssh.clients[i_client].hostname)
        ssh.run_cmd(i_client, commands)

    ssh.update("example/" + exp.name, str(settings.MEDIA_ROOT) + "current_experiment/" + exp.name)

    return HttpResponseRedirect(reverse('ping:play'))


def experiment_stop(request):
    client.publish("ihm/exit")
    client.publish("ihm/step")
    get_nohup()
    return HttpResponseRedirect(reverse('ping:play'))


def experiment_kill(request):
    exp = CurrentExperiment.objects.all()[0]

    if exp.amas_pid != 0:
        os.system("kill " + str(exp.amas_pid))
    if exp.env_pid != 0:
        os.system("kill " + str(exp.env_pid))
    if exp.scheduler_pid != 0:
        os.system("kill " + str(exp.scheduler_pid))
    return HttpResponseRedirect(reverse('ping:play'))


def experiment_metric(request, metrics_id):
    raw_metrics = Metrics.objects.filter(cycle=metrics_id)
    if len(raw_metrics) == 0:
        return JsonResponse({'data': []})
    else:
        cycle_metrics = [
            [metric.get("x"), metric.get("y"), metric.get("color")]
            for metric in raw_metrics[0].metrics if metric != {}]
        return JsonResponse({'data': cycle_metrics})


def export_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="users.csv"'

    writer = csv.writer(response)
    writer.writerow(['cycle'] + ["Agent_" + key for key in Metrics.objects.order_by('?').first().metrics[0].keys()])

    for m in Metrics.objects.all():
        cycle = m.cycle
        metrics = m.metrics
        for metric in metrics:
            if metric != {}:
                writer.writerow([cycle] + [value for value in metric.values()])

    return response
