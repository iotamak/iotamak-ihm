import pickle

from django.db import models
from django.forms import ModelForm


class Client(models.Model):
    hostname = models.CharField(max_length=16)
    username = models.CharField(max_length=16)
    password = models.CharField(max_length=16)
    status = models.CharField(max_length=8, default="Offline")
    active = models.BooleanField(default=True)

    def __str__(self):
        return "Hostname : " + self.hostname + " Username : " + self.username + " Status : " + self.status


class Agent(models.Model):
    ip = models.CharField(max_length=16)
    command = models.CharField(max_length=256)

    def __str__(self):
        return self.command


class Experiment(models.Model):
    name = models.CharField(max_length=60)
    status = models.CharField(max_length=60, default="Not checked")
    description = models.TextField()
    media = models.FileField(upload_to="media", null=True, blank=True)


class ExperimentForm(ModelForm):
    class Meta:
        model = Experiment
        fields = ["name", "description", "media"]


class Metrics(models.Model):
    cycle = models.PositiveIntegerField()
    _metrics = models.BinaryField()

    def set_data(self, data):
        self._metrics = pickle.dumps(data)

    def get_data(self):
        return pickle.loads(self._metrics)

    metrics = property(get_data, set_data)


class CurrentExperiment(models.Model):
    name = models.CharField(max_length=60)
    cycle = models.PositiveIntegerField(default=0)
    nbr_agent = models.PositiveIntegerField(default=0)

    amas_pid = models.PositiveIntegerField(default=0)
    scheduler_pid = models.PositiveIntegerField(default=0)
    env_pid = models.PositiveIntegerField(default=0)

    seed = models.CharField(default="0", max_length=64)
    scheduling = models.CharField(default="sync", max_length=10)

    canvas = models.BooleanField(default=False)
    canvas_height = models.PositiveIntegerField(default=1)
    canvas_width = models.PositiveIntegerField(default=1)

    com_graph = models.BooleanField(default=False)
    com_fun = models.CharField(default="linear", max_length=64)
    com_refresh = models.PositiveIntegerField(default=1)

    graph = models.BooleanField(default=False)
    graph_height = models.PositiveIntegerField(default=1)
    graph_width = models.PositiveIntegerField(default=1)
    graph_refresh = models.PositiveIntegerField(default=1)



class Network(models.Model):
    broker_ip = models.CharField(max_length=60)
    broker_username = models.CharField(max_length=60)
    broker_password = models.CharField(max_length=60)

    iotamak_core_version = models.CharField(max_length=60)

    path_to_iotamak = models.CharField(max_length=60)


class Link(models.Model):
    id1 = models.PositiveIntegerField(default=0)
    id2 = models.PositiveIntegerField(default=0)
    value = models.PositiveIntegerField(default=1)


class Node(models.Model):
    name = models.CharField(max_length=60)
    group = models.PositiveIntegerField(default=0)

class Canvas(models.Model):
    agent_id = models.PositiveIntegerField(default=0)

    x = models.PositiveIntegerField(default=0)
    y = models.PositiveIntegerField(default=0)

    color = models.CharField(max_length=60)

    cycle = models.PositiveIntegerField(default=0)

class GraphPoint(models.Model):

    name = models.CharField(max_length=60)
    cycle = models.PositiveIntegerField(default=0)
    y = models.IntegerField(default=0)